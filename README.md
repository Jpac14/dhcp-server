[![Codacy Badge](https://app.codacy.com/project/badge/Grade/896153f37c34493bb26df31544160dcd)](https://www.codacy.com/gl/Jpac14/dhcp-server/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=Jpac14/dhcp-server&amp;utm_campaign=Badge_Grade)

# DHCP Server

Wow! Another DHCP server. This DHCP was a fun project, to learn the inner workings of DHCP server. Probably shouldn't be used as a production DHCP server, but if you want you can.

## Questions

If you have any questions about the project or need and help with anything to do with the project, feel free to open an issue with the question template and I will try to answer ASAP.

## Contributing

If you are wanted to contribute to the project please read the CONTRIBUTING.md.

## To Do

- [ ] Check for broadcast bit
- [ ] Winsocks support
- [ ] Logging
- [ ] Refactor some names of variables and functions (some names are overlapping)
- [ ] Less `malloc` code
- [ ] Improve README.md with images and stats.
- [ ] Open Source Ready
  - [ ] Docker container
  - [ ] Wiki (config explaination, code overview, build instructions)
  - [ ] Style and formatting, linting, checking
  - [ ] Gitlab CI
- [ ] Tests (maybe later)
- [ ] Implement DHCPREQUEST + others
- [ ] Implement other allocation methods (e.g. dynamic)
- [ ] Other subnets support (giaddr paramater)
- [ ] More options (Read other related RFCs)
- [ ] Server identifier based off current listening IP address

## Considerations

- Is it better to compute the size of the packet, everytime it's needed or instead store the size in the packet struct (the latter is currently implemented)
