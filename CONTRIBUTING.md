# Contributing

If you would like to contribute to the project, it's super easy. Just make an issue and then a pull
request and start working on what you want to add, change or remove. Or if you want to fix and
existing issue, just make a pull request on and existing issue. I will try to review you pull
requests ASAP.
