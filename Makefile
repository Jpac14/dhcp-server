CC = clang-11

CCFLAGS = -g \
	-pedantic \
	-Weverything \
	-Wall \
	-Wextra \
	-Wno-unused-parameter \
	-fsanitize=undefined \
	-lpthread \
	-Isrc/include -I.

SRCS = $(wildcard src/*.c)
BIN = dhcp

run: build
	@echo "Running..."
	sudo ./$(BIN)

build:
	@echo "Building..."
	$(CC) $(SRCS) -o $(BIN) $(CCFLAGS)

clean:
	@echo "Cleaning..."
	rm $(BIN)

test:
	@echo "Testing..."

	@# Old test command
	@# @sudo dhcping -s 127.0.0.1 -h "34:e6:d7:0f:a9:83" -c 192.168.1.16 -V
	
	sudo sudo nmap --script broadcast-dhcp-discover -e lo

debug: build
	@echo "Debuging..."
	sudo gdb ./$(BIN)

valgrind: build
	@echo "Starting valgrind..."
	sudo valgrind --leak-check=full --show-leak-kinds=all ./dhcp

wireshark:
	sudo wireshark

.PHONY: run build clean test debug wireshark
