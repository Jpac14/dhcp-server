#include "stream.h"

u_int8_t read_uint8(stream *s) {
  u_int8_t val = s->buff[s->pos];
  s->pos += 1;
  return val;
}

u_int16_t read_uint16(stream *s) {
  u_int16_t val = (u_int16_t) (s->buff[s->pos] | s->buff[s->pos + 1] << 8);  
  s->pos += 2;
  return val;
}

u_int32_t read_uint32(stream *s) {
  u_int32_t val = (u_int32_t)
    (s->buff[s->pos] << 24 |
    s->buff[s->pos + 1] << 16 |
    s->buff[s->pos + 2] << 8 |
    s->buff[s->pos + 3]);
  s->pos += 4;
  return val;
}

void read_unsigned_byte_arr(stream *s, u_int8_t *arr, size_t size) {
  memcpy(arr, s->buff + s->pos, size);
  s->pos += size;
}

void read_byte_arr(stream *s, int8_t *arr, size_t size) {
  memcpy(arr, s->buff + s->pos, size);
  s->pos += size;
}

void write_uint8(stream *s, u_int8_t val) {
  s->buff[s->pos] = val;
  s->pos += 1;
}

void write_uint16(stream *s, u_int16_t val) {
  s->buff[s->pos] = val >> 8;
  s->buff[s->pos + 1] = (u_int8_t) val >> 0;
  s->pos += 2; 
}
 
void write_uint32(stream *s, u_int32_t val) {
  s->buff[s->pos] = (val >> 24);
  s->buff[s->pos + 1] = (val & 0x00FF0000) >> 16;
  s->buff[s->pos + 2] = (val & 0x0000FF00) >> 8;
  s->buff[s->pos + 3] = (val & 0x000000FF) >> 0;
  s->pos += 4;
}

void write_unsigned_byte_arr(stream *s, u_int8_t *arr, size_t size) {
  memcpy(s->buff + s->pos, arr, size);
  s->pos += size;
}

void write_byte_arr(stream *s, int8_t *arr, size_t size) {
  memcpy(s->buff + s->pos, arr, size);
  s->pos += size;
}

// Maybe remove this (to remove the malloc) and replace with local construction
stream *construct_stream(u_int8_t *buff, size_t pos) {
  stream *s = (stream *) malloc(sizeof(stream));

  s->buff = buff;
  s->pos = pos;

  return s;
}
