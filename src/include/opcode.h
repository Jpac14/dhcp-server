#ifndef OPCODE_H
#define OPCODE_H

// TODO Reformat names of enums
// https://github.com/krolaw/dhcp4/blob/master/packet.go

enum boot_opcode {
	BootRequest = 1, // From Client
	BootReply   = 2, // From Server
};

// DHCP Message Type 53
enum opcode {
	Discover = 1, // Broadcast Packet From Client - Can I have an IP?
	Offer    = 2, // Broadcast From Server - Here's an IP
	Request  = 3, // Broadcast From Client - I'll take that IP (Also start for renewals);
	Decline  = 4, // Broadcast From Client - Sorry I can't use that IP
	ACK      = 5, // From Server, Yes you can have that IP
	NAK      = 6, // From Server, No you cannot have that IP
	Release  = 7, // From Client, I don't need that IP anymore
	Inform   = 8, // From Client, I have this IP and there's nothing you can do about it
};

// DHCP Options
enum option_code {
	Pad                          = 0, 
	OptionSubnetMask             = 1,
	OptionTimeOffset             = 2,
	OptionRouter                 = 3, 
  OptionTimeServer             = 4, 
	OptionNameServer             = 5,
	OptionDomainNameServer       = 6,
	OptionLogServer              = 7,
	OptionCookieServer           = 8,
	OptionLPRServer              = 9,
	OptionImpressServer          = 10,
	OptionResourceLocationServer = 11,
	OptionHostName               = 12,
	OptionBootFileSize           = 13,
	OptionMeritDumpFile          = 14,
	OptionDomainName             = 15,
	OptionSwapServer             = 16,
	OptionRootPath               = 17,
	OptionExtensionsPath         = 18,

	// IP Layer Parameters per Host
	OptionIPForwardingEnableDisable          = 19,
	OptionNonLocalSourceRoutingEnableDisable = 20,
	OptionPolicyFilter                       = 21,
	OptionMaximumDatagramReassemblySize      = 22,
	OptionDefaultIPTimeToLive                = 23,
	OptionPathMTUAgingTimeout                = 24,
	OptionPathMTUPlateauTable                = 25,

	// IP Layer Parameters per Interface
	OptionInterfaceMTU              = 26,
	OptionAllSubnetsAreLocal        = 27,
	OptionBroadcastAddress          = 28,
	OptionPerformMaskDiscovery      = 29,
	OptionMaskSupplier              = 30,
	OptionPerformRouterDiscovery    = 31,
	OptionRouterSolicitationAddress = 32,
	OptionStaticRoute               = 33,

	// Link Layer Parameters per Interface
	OptionTrailerEncapsulation  = 34,
	OptionARPCacheTimeout       = 35,
	OptionEthernetEncapsulation = 36,

	// TCP Parameters
	OptionTCPDefaultTTL        = 37,
	OptionTCPKeepaliveInterval = 38,
	OptionTCPKeepaliveGarbage  = 39,

	// Application and Service Parameters
	OptionNetworkInformationServiceDomain            = 40,
	OptionNetworkInformationServers                  = 41,
	OptionNetworkTimeProtocolServers                 = 42,
	OptionVendorSpecificInformation                  = 43,
	OptionNetBIOSOverTCPIPNameServer                 = 44,
	OptionNetBIOSOverTCPIPDatagramDistributionServer = 45,
	OptionNetBIOSOverTCPIPNodeType                   = 46,
	OptionNetBIOSOverTCPIPScope                      = 47,
	OptionXWindowSystemFontServer                    = 48,
	OptionXWindowSystemDisplayManager                = 49,
	OptionNetworkInformationServicePlusDomain        = 64,
	OptionNetworkInformationServicePlusServers       = 65,
	OptionMobileIPHomeAgent                          = 68,
	OptionSimpleMailTransportProtocol                = 69,
	OptionPostOfficeProtocolServer                   = 70,
	OptionNetworkNewsTransportProtocol               = 71,
	OptionDefaultWorldWideWebServer                  = 72,
	OptionDefaultFingerServer                        = 73,
	OptionDefaultInternetRelayChatServer             = 74,
	OptionStreetTalkServer                           = 75,
	OptionStreetTalkDirectoryAssistance              = 76,

	OptionRelayAgentInformation = 82,

	// DHCP Extensions
	OptionRequestedIPAddress     = 50,
	OptionIPAddressLeaseTime     = 51,
	OptionOverload               = 52,
	OptionDHCP       			       = 53,
	OptionServerIdentifier       = 54,
	OptionParameterRequestList   = 55,
	OptionMessage                = 56,
	OptionMaximumDHCPMessageSize = 57,
	OptionRenewalTimeValue       = 58,
	OptionRebindingTimeValue     = 59,
	OptionVendorClassIdentifier  = 60,
	OptionClientIdentifier       = 61,

	OptionTFTPServerName = 66,
	OptionBootFileName   = 67,

	OptionUserClass = 77,

	OptionClientArchitecture = 93,

	OptionTZPOSIXString    = 100,
	OptionTZDatabaseString = 101,

	OptionDomainSearch = 119,

	OptionClasslessRouteFormat = 121,

	// From RFC3942 - Options Used by PXELINUX
	OptionPxelinuxMagic = 208,
	OptionPxelinuxConfigfile = 209,
	OptionPxelinuxPathprefix = 210,
	OptionPxelinuxReboottime = 211,
};

#endif
