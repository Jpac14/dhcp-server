#ifndef DHCP_PACKET_H
#define DHCP_PACKET_H

#include "stream.h"
#include "options.h"

typedef struct __attribute__((__packed__)) {
    u_int8_t op; // Message op code / message type 
    u_int8_t htype; // Hardware address type - "Assigned Numbers" RFC
    u_int8_t hlen; // Hardware address length
    u_int8_t hops; // Used by relay agents
    u_int32_t xid; // Transaction ID, random number chosen by client
    u_int16_t secs; // Seconds elapsed since process began, used?
    u_int16_t flags;
    u_int32_t ciaddr; // Client IP Address 
    u_int32_t yiaddr; // 'your' (client) IP address
    u_int32_t siaddr; // IP address of next server to use in bootstrap; DHCPOFFER, DHCPACK
    u_int32_t giaddr; // Relay agent IP address
    u_int8_t chaddr[6]; // Client hardware address
    u_int8_t chaddr_padding[10]; // Padding for client hardware address
    int8_t sname[64]; // Optional server host name, null terminated string.
    int8_t file[128]; // Boot file name, null terminated string
    u_int32_t mcookie; // Magic cookie
    option **options; // Optional parameters field
    size_t options_size; // Size of options, not used in response
    size_t size; // Total size of packet, not used in response
} dhcp_packet;

dhcp_packet read_dhcp_packet(u_int8_t buffer[], size_t len);
size_t size_of_dhcp_packet(dhcp_packet packet); 
u_int8_t *write_dhcp_packet(dhcp_packet packet);

#endif
