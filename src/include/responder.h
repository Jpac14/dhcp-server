#ifndef RESPONDER_H
#define RESPONDER_H

#include <stdbool.h> 
#include <stdio.h>

#include "dhcp_packet.h"
#include "options.h"
#include "opcode.h"
#include "stream.h"
#include "helper.h"
#include "config.h"

dhcp_packet respond(dhcp_packet);

void dhcp_discover(dhcp_packet request, dhcp_packet *response);
void dhcp_request(dhcp_packet request, dhcp_packet *response);
void dhcp_decline(dhcp_packet request, dhcp_packet *response);
void dhcp_release(dhcp_packet request, dhcp_packet *response);
void dhcp_inform(dhcp_packet request, dhcp_packet *response);

#endif
