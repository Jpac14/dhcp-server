#ifndef HELPER_H
#define HELPER_H

#include <stdlib.h>
#include <stdbool.h>
#include "stream.h"
#include "dhcp_packet.h"
#include "config.h"
#include "options.h"

u_int32_t u8_arr_ip_to_u32(u_int8_t *arr);

u_int32_t get_ip_from_hardware_addr(dhcp_packet request);
void add_options_from_request(option **option_arr, option *req_options, u_int8_t *hardware_addr);

#endif
