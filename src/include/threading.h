#ifndef THREADING_H
#define THREADING_H

#include <pthread.h>
#include <stdlib.h>
#include <netinet/in.h>

#include "net.h"

typedef struct __attribute__((packed)) {
  size_t len;
  u_int8_t *buffer;
  struct in_addr client_ip;
} request_args;

void *handle_request(void *request_args_void);
void create_thread_for_request(size_t len, u_int8_t *buffer, struct in_addr client_ip);

#endif
