#ifndef OPTIONS_H
#define OPTIONS_H

#include <string.h>

#include "stream.h"

typedef struct __attribute__((__packed__)) {
  u_int8_t code;
  u_int8_t len;
  u_int8_t *data;
} option;

option **read_options(u_int8_t *arr, size_t size);
u_int8_t *write_options(option **option_arr);

option **construct_option_arr(void);

void construct_option(option **option_arr, u_int8_t option_code, u_int8_t len, u_int8_t *data);
void construct_option_with_str(option **option_arr, u_int8_t option_code, char *str);
void construct_option_with_u8(option **option_arr, u_int8_t option_code, u_int8_t val);

size_t size_of_options(option **option_arr);
void free_options(option **options);

#endif
