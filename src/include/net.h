#ifndef NET_H
#define NET_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>

#include "dhcp_packet.h"
#include "responder.h"
#include "options.h"
#include "threading.h"

int send_response(u_int8_t *packet, size_t packet_len, struct in_addr host);
__attribute__((noreturn)) void setup_server(void);

#endif
