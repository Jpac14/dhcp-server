#include "dhcp_packet.h"

dhcp_packet read_dhcp_packet(u_int8_t buffer[], size_t len) {
  dhcp_packet new;
  stream *s = construct_stream(buffer, 0);

  new.op = read_uint8(s);
  new.htype = read_uint8(s);
  new.hlen = read_uint8(s);
  new.hops = read_uint8(s);
  new.xid = read_uint32(s);
  new.secs = read_uint16(s);
  new.flags = read_uint16(s);
  
  new.ciaddr = read_uint32(s);
  new.yiaddr = read_uint32(s);
  new.siaddr = read_uint32(s);
  new.giaddr = read_uint32(s);

  read_unsigned_byte_arr(s, new.chaddr, 6);
  read_unsigned_byte_arr(s, new.chaddr_padding, 10);
  read_byte_arr(s, new.sname, 64);
  read_byte_arr(s, new.file, 128);
  new.mcookie = read_uint32(s);
  
  size_t end = len - s->pos;
  u_int8_t *options_buff = (u_int8_t *) malloc(sizeof(u_int8_t) * end);

  read_unsigned_byte_arr(s, options_buff, end);
  new.options = read_options(options_buff, end);

  free(options_buff);

  new.options_size = size_of_options(new.options);
  new.size = size_of_dhcp_packet(new); 

  free(s);
  
  return new;
}

size_t size_of_dhcp_packet(dhcp_packet packet) {
  size_t total = 0;
  
  total += sizeof(packet);
  total -= sizeof(packet.options);
  total -= sizeof(packet.options_size);
  total -= sizeof(packet.size);
  total += size_of_options(packet.options);

  return total;
}

u_int8_t *write_dhcp_packet(dhcp_packet packet) {  
  u_int8_t *buff = (u_int8_t *) malloc(packet.size * sizeof(u_int8_t));
  stream *s = construct_stream(buff, 0);

  write_uint8(s, packet.op);
  write_uint8(s, packet.htype);
  write_uint8(s, packet.hlen);
  write_uint8(s, packet.hops);

  write_uint32(s, packet.xid);
  write_uint16(s, packet.secs);
  write_uint16(s, packet.flags);
  
  write_uint32(s, packet.ciaddr);
  write_uint32(s, packet.yiaddr);
  write_uint32(s, packet.siaddr);
  write_uint32(s, packet.siaddr);

  write_unsigned_byte_arr(s, packet.chaddr, 6);
  write_unsigned_byte_arr(s, packet.chaddr_padding, 10);
  write_byte_arr(s, packet.sname, 64);
  write_byte_arr(s, packet.file, 128);
  write_uint32(s, packet.mcookie);
 
  u_int8_t *options_buff = write_options(packet.options);
  write_unsigned_byte_arr(s, options_buff, packet.options_size);
  free(options_buff);
  
  free(s);

  return buff;
}
