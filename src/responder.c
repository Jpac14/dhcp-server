#include "responder.h"

dhcp_packet respond(dhcp_packet request) {
  printf("Requested Recieved\n");

  // Probably not a good idea?? TODO Make a method to start response.
  dhcp_packet response = request;
  response.op = BootReply;

  enum opcode code = (enum opcode) request.options[OptionDHCP]->data[0];

  option **option_arr = construct_option_arr();
  response.options = option_arr;

  // Universal option
  construct_option(option_arr, OptionServerIdentifier, 4, server_identifier);

  switch (code) {
    case Discover:
      dhcp_discover(request, &response);
      break;
    case Request:
      dhcp_request(request, &response);
      break;
    case Decline:
      dhcp_decline(request, &response);
      break;
    case Release:
      dhcp_release(request, &response);
      break;
    case Inform:
      dhcp_inform(request, &response);
      break;

    case Offer:
    case ACK:
    case NAK:
      // Requests sent by server not client
      break;
  }

  response.options_size = size_of_options(response.options);
  response.size = size_of_dhcp_packet(response);

  return response;
}

void dhcp_discover(dhcp_packet request, dhcp_packet *response) {
  option **option_arr = response->options;
  add_options_from_request(option_arr, request.options[OptionParameterRequestList], response->chaddr);
  
  response->yiaddr = get_ip_from_hardware_addr(request);

  construct_option_with_u8(option_arr, OptionDHCP, Offer);

  u_int8_t lease_time_data[] = {0xFF, 0xFF, 0xFF, 0xFF};
  construct_option(option_arr, OptionIPAddressLeaseTime, 4, lease_time_data);

  construct_option_with_str(option_arr, OptionMessage, "Manual IP allocation successful");
}

// Do I return the requested paramaters on a DHCPNAK?
void dhcp_request(dhcp_packet request, dhcp_packet *response) {
  option **option_arr = response->options;
  option **req_options = request.options;

  // SELECTING state 
  if (req_options[OptionServerIdentifier]->data == server_identifier
      && request.ciaddr == 0 && req_options[OptionRequestedIPAddress] != 0) {
    u_int32_t ip = get_ip_from_hardware_addr(request);
    u_int32_t requested_ip = u8_arr_ip_to_u32(req_options[OptionRequestedIPAddress]->data);

    if (ip == requested_ip) {
      add_options_from_request(option_arr, req_options[OptionParameterRequestList], response->chaddr);

      response->yiaddr = ip; 

      construct_option_with_u8(option_arr, OptionDHCP, ACK);

      u_int8_t lease_time_data[] = {0xFF, 0xFF, 0xFF, 0xFF};
      construct_option(option_arr, OptionIPAddressLeaseTime, 4, lease_time_data);

      construct_option_with_str(option_arr, OptionMessage, "Manual IP allocation successful");
    } else {
      construct_option_with_u8(option_arr, OptionDHCP, NAK);

      construct_option_with_str(option_arr, OptionMessage, "Incorrect IP requested");
    }
    
    // Is SELECTING and INIT-REBOOT, the same?
    // TODO Implement other DHCPREQUEST states, e.g. INIT-REBOOT, RENEWING, REBINDING
  }
}

void dhcp_decline(dhcp_packet request, dhcp_packet *response) {
  // TODO Implement this
}

void dhcp_release(dhcp_packet request, dhcp_packet *response) {
  // TODO Implement this
}

void dhcp_inform(dhcp_packet request, dhcp_packet *response) {
  // TODO Implement this
}
