#include "helper.h"

u_int32_t u8_arr_ip_to_u32(u_int8_t *arr) {
  stream *s = construct_stream(arr, 0);
  u_int32_t val = read_uint32(s);

  free(s);

  return val;
}

u_int32_t get_ip_from_hardware_addr(dhcp_packet request) {
  int allocations_len = sizeof(allocations) / sizeof(allocations[0]);

  for (int i = 0; i < allocations_len; i++) {
    if (memcmp(request.chaddr, allocations[i].hardware_addr, 6) == 0) {
      u_int32_t ip = u8_arr_ip_to_u32(allocations[i].ip_address);

      return ip;
    }
  }

  return 0;
}

void add_options_from_request(option **option_arr, option *req_options, u_int8_t *hardware_addr) { 
  // Checks if hardware address has local configuration
  int local_options_len = sizeof(local_options_map) / sizeof(local_options_map[0]);
  bool has_local_options = false;
  int index = 0;

  for (int i = 0; i < local_options_len; i++) {
    if (memcmp(hardware_addr, local_options_map[i].hardware_addr, 6) == 0) {
      has_local_options = true;
      index = i;
      break;
    }
  }

  if (has_local_options) {
    for (int i = 0; i < local_options_map[index].len; i++) {
      option current = local_options_map[index].options[i];
      for (int j = 0; j < req_options->len; j++) {
        if (req_options->data[j] == local_options_map[index].options[i].code) {
          construct_option(option_arr, current.code, current.len, current.data);
        }
      }
    }
  } else {
    int global_options_len = sizeof(global_options) / sizeof(global_options[0]);
    for (int i = 0; i < global_options_len; i++) {
      option current = global_options[i];
      for (int j = 0; j < req_options->len; j++) {
        if (req_options->data[j] == global_options[i].code) {
          construct_option(option_arr, current.code, current.len, current.data);
        }
      }
    }
  }
}
