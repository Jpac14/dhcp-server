#include "net.h"

#define BUFFER_SIZE 1024

#define SERVER_PORT 67
#define CLIENT_PORT 68

int send_response(u_int8_t *packet, size_t packet_len, struct in_addr host) {
  int socket_fd;
  struct sockaddr_in reciever;

  if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  memset(&reciever, 0, sizeof(reciever));

  reciever.sin_family = AF_INET;
  reciever.sin_port = htons(CLIENT_PORT);
  reciever.sin_addr = host;

  sendto(socket_fd, packet, packet_len,
         MSG_CONFIRM, (const struct sockaddr *)&reciever,
         sizeof(reciever));

  close(socket_fd);
  free(packet);

  return 0;
}

__attribute__((noreturn)) void setup_server(void) {
  int socket_fd;
  struct sockaddr_in server_addr, client_addr;

  if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
      perror("socket creation failed");
      exit(EXIT_FAILURE);
  }

  memset(&server_addr, 0, sizeof(server_addr));
  memset(&client_addr, 0, sizeof(client_addr));

  server_addr.sin_family = AF_INET;
  server_addr.sin_addr.s_addr = INADDR_ANY;
  server_addr.sin_port = htons(SERVER_PORT);

  if (bind(socket_fd, (const struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
      perror("bind failed");
      exit(EXIT_FAILURE);
  }

  unsigned int client_addr_len = sizeof(client_addr);
  u_int8_t buffer[BUFFER_SIZE] = {0};

  for (;;) {
    ssize_t len = recvfrom(socket_fd, &buffer, sizeof(buffer), MSG_WAITALL,
                   (struct sockaddr *)&client_addr, &client_addr_len);

    if (len == -1) {
      continue;
    }
    

    u_int8_t *temp_buffer = (u_int8_t *) malloc(sizeof(u_int8_t) * (unsigned long) len);
    memcpy(temp_buffer, buffer, (unsigned long) len);
    create_thread_for_request((size_t) len, temp_buffer, client_addr.sin_addr);

    memset(buffer, 0, BUFFER_SIZE);
  }
}
